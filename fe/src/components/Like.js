import React, {  useState } from "react";
import { useSelector } from "react-redux";
import PageService from "../services/page.service";
import { FaRegThumbsUp, FaRegThumbsDown } from "react-icons/fa";

const Like = (props) => {
  const { page } = props;
  const { isLoggedIn } = useSelector(state => state.auth);
  const [likeCount, setLikeCount] = useState(page.like);
  const [dislikeCount, seDislikeCount] = useState(page.dislike);
  
  const onLike = (id) => {
    PageService.vote(id, true).then(
      (response) => {
        if(response.data) {
          setLikeCount(response.data.like);
          seDislikeCount(response.data.dislike);
        }
      },
      (error) => {
        
      }
    );
  };

  const onDisLike = (id) => {
    PageService.vote(id, false).then(
      (response) => {
        if(response.data) {
          setLikeCount(response.data.like);
          seDislikeCount(response.data.dislike);
        }
      },
      (error) => {
        
      }
    );
  };

  return (
      
      <div className="row col-sm-12">
        <div className="col-sm-3">
        {likeCount??0} {isLoggedIn && (<button className="button-like"><span  onClick={() => onLike(page._id)}><FaRegThumbsUp /></span></button>) }
        {!isLoggedIn && (<button className="button-like" disabled><span ><FaRegThumbsUp /></span></button>) }
        </div>
        <div className="col-sm-3">
        {dislikeCount??0} {isLoggedIn && (<button className="button-like"><span  onClick={() => onDisLike(page._id)}><FaRegThumbsDown /></span></button>) }
        {!isLoggedIn && (<button className="button-like" disabled><span ><FaRegThumbsDown /></span></button>) }
        </div>
      </div>
  );
};

export default Like;

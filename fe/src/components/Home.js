import React, {  useState, useEffect } from "react";
import PageService from "../services/page.service";
import ReactHtmlParser from 'react-html-parser'; 
import Like from "./Like";

const Home = (props) => {
  const [pages, setPages] = useState("");

  useEffect(() => {
    PageService.getAll().then(
      (response) => {
        setPages(response.data);
      },
      (error) => {
        
      }
    );
  }, []);

  return (
    <div className="container">
    {
      (pages && (pages.map((page) => 
        (
          <div className="row">
            <div className="col-sm-6">
            { page.html && (ReactHtmlParser (page.html.replace(`width="200" height="113"`, `width="400" height="225"`)))}
            </div>

            <div className="col-sm-6">
              <div className="row">
                <div className="col-sm-12 page-tile">
                  {page.title}
                </div>
                <Like page={page}/>
                <div className="col-sm-12">
                  <strong>Share by</strong>: {page.share_by}
                </div>
                <br/>
                <div className="col-sm-12">
                  <strong>Description</strong>
                  <br/>
                  {page.description}
                </div>
              </div>
            </div>
          </div>
        )
      )))
    }
    </div>
  );
};

export default Home;

import React, { useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from 'react-router-dom';
import axios from "axios";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isURL } from "validator";
import { create } from "../actions/page";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Share = (props) => {
  const form = useRef();
  const checkBtn = useRef();

  const [url, setUrl] = useState("");
  const [loading, setLoading] = useState(false);

  const { isLoggedIn, user } = useSelector(state => state.auth);
  const { message } = useSelector(state => state.message);

  const dispatch = useDispatch();

  const validUrl = (value) => {
    if (!isURL(value)) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a valid Url.
        </div>
      );
    }
    if (value && !value.includes("youtube.com")) {
      return (
        <div className="alert alert-danger" role="alert">
          This is not a youtube Url.
        </div>
      );
    }
  };
  
  const onChangeUrl = (e) => {
    const url = e.target.value;
    setUrl(url);
  };


  const handleShare = (e) => {
    e.preventDefault();

    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      const jsonUrl = `https://www.youtube.com/oembed?url=${url}&format=json`;
      axios.get(jsonUrl).then(resp => {
        if(resp.data) {
          dispatch(create({ share_by: user.username, url, title: resp.data.title, description: `Video: ${resp.data.title} author: ${resp.data.author_name}`, html: resp.data.html}))
            .then(() => {
              setLoading(false);
            })
            .catch(() => {
              setLoading(false);
            });
        }
        return Promise.resolve();
      });
    } else {
      setLoading(false);
    }
  };

  if (!isLoggedIn) {
    return <Redirect to="/" />;
  }

  return (
    <div className="col-md-12">
      <div className="share share-container">
        <Form onSubmit={handleShare} ref={form}>
          <div className="form-group">
            <label htmlFor="url">Youtube URL</label>
            <Input
              type="text"
              className="form-control"
              name="url"
              value={url}
              onChange={onChangeUrl}
              validations={[required, validUrl]}
            />
          </div>
         
          <div className="form-group">
            <button className="btn btn-primary btn-block" disabled={loading}>
              {loading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}
              <span>Share</span>
            </button>
          </div>
          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
    </div>
  );
};

export default Share;

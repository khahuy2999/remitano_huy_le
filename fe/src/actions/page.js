import {
  PAGE_CREATE_SUCCESS,
  PAGE_CREATE_FAIL,
  SET_MESSAGE
  } from "./types";
  import PageService from "../services/page.service";
  
  export const create = (page) => (dispatch) => {
    return PageService.create(page).then(
      (response) => {
        dispatch({
          type: PAGE_CREATE_SUCCESS,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: response.data.message,
        });
  
        return Promise.resolve();
      },
      (error) => {
        const message =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();
  
        dispatch({
          type: PAGE_CREATE_FAIL,
        });
  
        dispatch({
          type: SET_MESSAGE,
          payload: message,
        });
  
        return Promise.reject();
      }
    );
  };
  
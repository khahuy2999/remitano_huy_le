import axios from "axios";
import authHeader from "./auth-header";
const API_URL = `http://${window.location.hostname}:8080/api/page/`;
const create = (page) => {
  return axios.post(API_URL + "create", page, { headers: authHeader() });
};
const getAll = () => {
  return axios.get(API_URL + "all", { headers: authHeader() });
};
const vote = (id, like) => {
  return axios.put(API_URL + "vote", {id, like}, { headers: authHeader() });
};
export default {
  create,
  getAll,
  vote
};
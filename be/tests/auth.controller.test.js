//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block

describe("auth_controller", () => {
    describe('/POST signin', () => {
        it('it should POST a sign in successfully', (done) => {
            let body = {
                username: "admin11@admin.com",
                password: "admin@123"
            };
            chai.request(server)
                .post('/api/auth/signin', body)
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.user.should.have.property('id');
                    res.body.user.should.have.property('username').eql(body.username);
                    res.body.user.should.have.property('accessToken');
                    done();
                });
        });  
        it('it should POST a sign in - Invalid Password!', (done) => {
            let body = {
                username: "admin11@admin.com",
                password: "admin@1234"
            };
            chai.request(server)
                .post('/api/auth/signin', body)
                .send(body)
                .end((err, res) => {
                    res.should.have.status(401);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql("Invalid Password!");
                    res.body.should.have.property('accessToken').eql(null);
                    done();
                });
        });          
    });

    
    describe("auth_controller.signup", () => {
        
    });
});


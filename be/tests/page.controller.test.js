//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
const sinon = require('sinon');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

const config = require("../app/config/auth.config");
var jwt = require("jsonwebtoken");
let token = jwt.sign({ id: '62cc222308b3918f56faafce' }, config.secret, {
    expiresIn: 86400, // 24 hours
  });


describe("page_controller", () => {
    describe('/POST create', () => {
        const sandbox = sinon.createSandbox();
        beforeEach(() => {
            const pageList = new Page(db, id, logger);
          })

        it('it should POST a page successfully', (done) => {
            let body = {
                title: "title",
                share_by: "admin1@admin.com",
                description: "description",
                url:  "https://www.youtube.com/watch?v=ITNy1Rl2fp1",
                html: "html"
              };
            chai.request(server)
                .post('/api/page/create', body)
                .set({ "x-access-token": `${token}` })
                .send(body)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Youtube video was shared successfully!')
                    done();
                });
        }); 
    });
});
const { authJwt } = require("../middlewares");
const controller = require("../controllers/page.controller");
module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.get("/api/page/all", controller.getAll);
  app.post("/api/page/create", [authJwt.verifyToken], controller.create);
  app.put("/api/page/vote", [authJwt.verifyToken], controller.vote);
};
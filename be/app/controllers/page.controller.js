const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Page = db.page;
const Like = db.like;

exports.create = (req, res) => {
  Page.findOne({
    url:  req.body.url,
  })
  .exec((err, page) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!page) {
      page = new Page({
        title: req.body.title,
        share_by: req.body.share_by,
        description: req.body.description,
        url:  req.body.url,
        html: req.body.html
      });
      page.save((err, page) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        
        res.send({ message: "Youtube video was shared successfully!" });
      });
    } else {
      res.send({ message: "Youtube Url exists!" });
    }
  });
};

exports.getAll = (req, res) => {
  Page.find({})
    .exec((err, pages) => {
      if (err) {
        res.status(500).send({ message: err });
        return;
      }
      if(req.userId) {
        Like.find({ user: req.userId })
        .exec((err, likes) => {
          if (err) {
            res.status(500).send({ message: err });
            return;
          }
          if(likes) {
            pages = pages.map((page) => {
              var like = likes.find((l) => l.page === page._id) 
              if(like) 
                page.isLike = like.like;
              return page;
            });
          }
          res.status(200).send(pages);
        });
      }
      else 
      {
        
      }
      res.status(200).send(pages);
    });
};

exports.vote = (req, res) => {
  Page.findOne({
    _id: req.body.id,
  })
  .exec((err, page) => {
    if (err) {
      res.status(500).send({ message: err });
      return;
    }
    if (!page) {
      res.send({ message: "Not found page !" });
    } else {
      if(req.body.like)
      {
        page.like = (page.like??0) + 1;
      } else {
        page.dislike = (page.dislike??0) + 1;
        
      }

      Like.findOne({
        user: req.userId,
      })
      .exec((err, like) => {
        if (err) {
          res.status(500).send({ message: err });
          return;
        }
        if (!like) { 
          const like = new Like({
            user: req.userId,
            page: req.body.id,
            like: req.body.like,
          });
          like.save((err, like) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
  
            page.save((err) => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }
              res.status(200).send({ like: page.like,  dislike: page.dislike});
            });
          });
        } else {
          if(req.body.like)
          {
            page.dislike = (page.dislike === undefined || page.dislike <= 0) ? 0 : page.dislike - 1;
          } else {
            page.like =  page.like === undefined || page.like <= 0 ? 0 : page.like - 1;
          }
          like.like = req.body.like;
          like.save((err) => {
            if (err) {
              res.status(500).send({ message: err });
              return;
            }
            page.save((err) => {
              if (err) {
                res.status(500).send({ message: err });
                return;
              }
              res.status(200).send({ like: page.like,  dislike: page.dislike});
            });
          });
        }
      });
    }
  });
};
const mongoose = require("mongoose");
const Like = mongoose.model(
  "Like",
  new mongoose.Schema({
    page:  {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Page"
    },
    user:  {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
    like: Boolean
  })
);
module.exports = Like;
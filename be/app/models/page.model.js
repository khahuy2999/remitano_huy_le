const mongoose = require("mongoose");
const Page = mongoose.model(
  "Page",
  new mongoose.Schema({
    title: String,
    share_by: String,
    description: String,
    like: Number,
    dislike: Number,
    url: String,
    html: String
  })
);
module.exports = Page;
const express = require("express");
const cors = require("cors");
const app = express();
let morgan = require('morgan');
//don't show the log when it is test
if(process.env.NODE_ENV !== 'test') {
  //use morgan to log at command line
  app.use(morgan('combined')); //'combined' outputs the Apache style LOGs
}

var corsOptions = {
  origin:  ["http://localhost:8081","http://localhost:3000","https://ec2-54-209-185-5.compute-1.amazonaws.com:8081","https://ec2-54-209-185-5.compute-1.amazonaws.com:3000","http://ec2-54-209-185-5.compute-1.amazonaws.com:8081","http://ec2-54-209-185-5.compute-1.amazonaws.com:3000"]
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));
// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome." });
});
// set port, listen for requests
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

const dbConfig = require("./app/config/db.config");
const db = require("./app/models");
const Role = db.role;
db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Successfully connect to MongoDB.");
    initial();
  })
  .catch(err => {
    console.error("Connection error", err);
    process.exit();
  });

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "admin"
      }).save(err => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}

require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/page.routes')(app);

module.exports = app